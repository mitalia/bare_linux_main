all: bare32 bare64

bare32: bare.c
	gcc -O3 -static -Wall -Wextra -m32 -g -nostdlib bare.c -o bare32

bare64: bare.c
	gcc -O3 -static -Wall -Wextra -g -nostdlib bare.c -o bare64

clean:
	rm -f bare32 bare64
